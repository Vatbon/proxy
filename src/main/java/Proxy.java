import org.xbill.DNS.ResolverConfig;

import java.io.EOFException;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.channels.spi.SelectorProvider;
import java.nio.charset.StandardCharsets;
import java.util.*;

class Proxy implements Runnable {

    private SelectionKey dnsKey;
    final Map<Short, SelectionKey> dnsMap = new HashMap<>();
    Selector selector;

    Queue<ByteBuffer> queue = new ArrayDeque<>();

    final int bufferSize = 8192;
    int port;
    String host;
    final Logger logger = new Logger(System.out);

    static class Attachment {

        boolean usingDns = false;
        boolean isDns = false;
        boolean awaits = false;
        boolean resolved = false;
        int port;
        byte[] unresolvedAddress;
        byte[] resolvedAddress;

        ByteBuffer in;
        ByteBuffer out;
        SelectionKey peer;

    }

    static final byte[] METHOD = new byte[]{0x05, 0x00};
    static final byte[] NOMETHOD = new byte[]{0x05, (byte) 0xFF};

    private byte[] makeOK(SelectionKey key) {
        Attachment attachment = (Attachment) key.attachment();
        Socket socket = ((SocketChannel) key.channel()).socket();
        if (!attachment.usingDns) {
            byte[] address = attachment.resolvedAddress;
            int socketPort = socket.getPort();
            return new byte[]{0x05, 0x00, 0x00, 0x01, address[0], address[1], address[2], address[3], (byte) (socketPort & 0xFF), (byte) (socketPort >> 8 & 0xFF)};
        } else {
            byte[] address = attachment.unresolvedAddress;
            int socketPort = socket.getPort();
            return ByteBuffer.allocate(7 + address.length).put(new byte[]{0x05, 0x00, 0x00, 0x03})
                    .put((byte) address.length)
                    .put(address)
                    .put(new byte[]{(byte) (socketPort & 0xFF), (byte) (socketPort >> 8 & 0xFF)})
                    .array();
        }
    }

    @Override
    public void run() {
        try (
                ServerSocketChannel serverChannel = ServerSocketChannel.open()
        ) {
            // Создаём Selector
            selector = SelectorProvider.provider().openSelector();
            serverChannel.configureBlocking(false);
            serverChannel.socket().bind(new InetSocketAddress(host, port));
            serverChannel.register(selector, serverChannel.validOps());
            //сокет для днс
            registerDNSSocket(selector);
            while (true) {
                while (selector.select(1000) > 0) {
                    logger.log("--");
                    Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
                    while (iterator.hasNext()) {
                        SelectionKey key = iterator.next();
                        iterator.remove();
                        if (key.isValid()) {
                            try {
                                if (key.isAcceptable()) {
                                    // Принимаем соединение
                                    accept(key);
                                } else if (key.isConnectable()) {
                                    // Устанавливаем соединение
                                    connect(key);
                                } else if (key.isReadable()) {
                                    // Читаем данные
                                    read(key);
                                } else if (key.isWritable()) {
                                    // Пишем данные
                                    write(key);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                close(key);
                            }
                        }
                    }
                    break;
                }
                if (!queue.isEmpty()) {
                    logger.log("checking queue. SIZE = " + queue.size());
                    Attachment attachment = (Attachment) dnsKey.attachment();
                    if ((dnsKey.interestOps() & SelectionKey.OP_WRITE) == 0) {
                        attachment.out = queue.poll();
                        dnsKey.interestOps(SelectionKey.OP_WRITE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            this.closeOnException();
        }
    }

    private void closeOnException() {
        dnsKey.cancel();
        logger.log("EXITING ON EXPECTION");
        try {
            dnsKey.channel().close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            selector.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void registerDNSSocket(Selector selector) {
        try (
                DatagramSocket datagramSocket = new DatagramSocket();//WTF it just works
        ) {
            String[] dnsServer = ResolverConfig.getCurrentConfig().servers();
            if (dnsServer.length > 0) {
                DatagramChannel dnsChannel = selector.provider().openDatagramChannel();
                dnsChannel.socket().connect(InetAddress.getByName(dnsServer[0]), 53);
                dnsChannel.configureBlocking(false);
                dnsChannel.register(selector, SelectionKey.OP_READ);
                Attachment attachment = new Attachment();
                attachment.in = ByteBuffer.allocate(bufferSize);
                attachment.out = ByteBuffer.allocate(bufferSize);
                attachment.isDns = true;
                attachment.awaits = false;
                attachment.resolved = true;
                SelectionKey key = (SelectionKey) selector.keys().toArray()[0];
                key.interestOps(0);
                key.attach(attachment);
                dnsKey = key;
            } else
                throw new Exception("oh no!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void accept(SelectionKey key) throws IOException {
        logger.log("accept");
        SocketChannel newChannel = ((ServerSocketChannel) key.channel()).accept();
        newChannel.configureBlocking(false);
        newChannel.register(key.selector(), SelectionKey.OP_READ);
    }

    private void read(SelectionKey key) throws IOException {
        logger.log("read");
        Attachment attachment = ((Attachment) key.attachment());
        if (attachment == null) {
            // Лениво инициализируем буферы
            attachment = new Attachment();
            key.attach(attachment);
            attachment.in = ByteBuffer.allocate(bufferSize);
        }
        if (attachment.isDns) {
            logger.log("read dns");
            DatagramChannel channel1 = (DatagramChannel) key.channel();
            int len = channel1.read(attachment.in);
            if (len == 0) {
                attachment.in.clear();
                key.interestOps(key.interestOps() ^ SelectionKey.OP_READ);
                return;
            }
            System.out.println(len);
            byte[] d = attachment.in.array();
            for (int i = 0; i < len; i++) {
                System.out.print(" 0x" + String.format("%x", d[i]) + " ");
            }
            try {
                byte[] dnsAddr = Resolver.getDNSName(attachment.in.array());
                byte[] resolvedAddr = Resolver.getResolvedAddress(attachment.in.array());
                System.out.println(Arrays.toString(resolvedAddr));
                System.out.println(new String(dnsAddr, StandardCharsets.UTF_8));
                short id = attachment.in.getShort(0);
                SelectionKey key1 = dnsMap.get(id);
                System.out.println("FOUND ID IS = " + id);
                if (key1 != null) {
                    Attachment attachment1 = (Attachment) key1.attachment();
                    attachment1.resolvedAddress = resolvedAddr;
                    attachment1.resolved = true;
                    makeConnection(key1, attachment1);
                    d = Resolver.getDNSName(attachment.in.array());
                    for (int i = 0; i < d.length; i++) {
                        System.out.print(" 0x" + String.format("%x", d[i]) + " ");
                    }
                    d = Resolver.getResolvedAddress(attachment.in.array());
                    for (int i = 0; i < d.length; i++) {
                        System.out.print(" 0x" + String.format("%x", d[i]) + " ");
                    }
                    dnsMap.remove(attachment.in.getShort());
                } else
                    System.out.println("DNS MISS");
            } catch (EOFException e) {
                e.printStackTrace();
            } finally {
                attachment.in.clear();
            }
            return;
        }

        SocketChannel channel = ((SocketChannel) key.channel());
        if (channel.read(attachment.in) < 1) {
            close(key);
        } else if (attachment.awaits) {
            makeConnection(key, attachment);
        } else if (attachment.peer == null) {
            readHeader(key, attachment);
        } else {
            attachment.peer.interestOps(attachment.peer.interestOps() | SelectionKey.OP_WRITE);
            key.interestOps(key.interestOps() ^ SelectionKey.OP_READ);
            attachment.in.flip();
        }
    }

    private void makeConnection(SelectionKey key, Attachment attachment) throws IOException {
        logger.log("making connection");
        if (!attachment.resolved) {
            byte[] ar = attachment.in.array();
            if (ar[0] != 0x05 || ar[1] != 0x01 || (ar[3] != 0x01 && ar[3] != 0x03)) {
                throw new IllegalStateException("Bad Request");
            } else {
                // Получаем из пакета адрес и порт
                if (ar[3] == 0x01) {
                    attachment.unresolvedAddress = null;
                    attachment.resolvedAddress = new byte[]{ar[4], ar[5], ar[6], ar[7]};
                    attachment.resolved = true;
                    attachment.port = ((0xFF & ar[8]) << 8) + (0xFF & ar[9]);
                } else if (ar[3] == 0x03) {//DNS
                    int portPos = 5 + ar[4];
                    attachment.port = ((0xFF & ar[portPos]) << 8) + (0xFF & ar[portPos + 1]);
                    attachment.unresolvedAddress = Arrays.copyOfRange(ar, 5, 5 + (short) ar[4]);
                    attachment.resolvedAddress = null;
                    attachment.resolved = false;
                    attachment.awaits = false;
                    attachment.usingDns = true;
                    resolveDNS(key, attachment.unresolvedAddress);
                    return;
                } else {
                    throw new IllegalStateException("Bad Request");
                }
            }
        }
        // Начинаем устанавливать соединение
        if (attachment.resolvedAddress != null && key.isValid()) {
            SocketChannel peer = SocketChannel.open();
            peer.configureBlocking(false);
            peer.connect(new InetSocketAddress(InetAddress.getByAddress(attachment.resolvedAddress), attachment.port));
            SelectionKey peerKey = peer.register(key.selector(), SelectionKey.OP_CONNECT);
            key.interestOps(0);
            attachment.awaits = false;
            attachment.resolved = true;
            attachment.peer = peerKey;
            Attachment peerAttachment = new Attachment();
            peerAttachment.usingDns = attachment.usingDns;
            peerAttachment.resolvedAddress = attachment.resolvedAddress;
            peerAttachment.unresolvedAddress = attachment.unresolvedAddress;
            peerAttachment.peer = key;
            peerKey.attach(peerAttachment);
            attachment.in.clear();
        }

    }

    private void resolveDNS(SelectionKey key, byte[] data) {
        System.out.println(new String(data, StandardCharsets.UTF_8));
        queue.add(ByteBuffer.wrap(Resolver.makeAMessage(data)));
        dnsMap.put(queue.peek().position(0).getShort(), key);
        System.out.println("ID IS = " + queue.peek().position(0).getShort());
        queue.peek().position(0);
    }

    private void readHeader(SelectionKey key, Attachment attachment) throws IllegalStateException, IOException {
        logger.log("reading header");
        byte[] ar = attachment.in.array();
        if (ar[0] != 0x05 || ar[1] < 1) {
            throw new IllegalStateException("Bad Request");
        } else {

            //looking for supported method
            int i = 2;
            for (; i - 2 < ar[1]; i++) {
                if (ar[i] == 0) {
                    i = 0;
                    break;
                }
            }

            if (i != 0) {
                ((SocketChannel) key.channel()).write(ByteBuffer.wrap(NOMETHOD).flip());
                throw new IllegalStateException("Supported method no found");
            } else {
                attachment.awaits = true;
                attachment.in.clear().put(METHOD).flip();
                ((SocketChannel) key.channel()).write(attachment.in);
                key.interestOps(SelectionKey.OP_READ);
                attachment.in.clear();
            }
        }
    }

    private void write(SelectionKey key) throws IOException {
        Attachment attachment = ((Attachment) key.attachment());
        if (attachment.isDns) {
            logger.log("write dns");
            DatagramChannel channel = (DatagramChannel) key.channel();
            int len = channel.write(attachment.out);
            byte[] d = attachment.out.array();
            for (int i = 0; i < len; i++) {
                System.out.print(" 0x" + String.format("%x", d[i]) + " ");
            }
            attachment.out.clear();
            key.interestOps(SelectionKey.OP_READ);
        } else {
            logger.log("write normal");
            SocketChannel channel = ((SocketChannel) key.channel());
            if (channel.write(attachment.out) == -1) {
                close(key);
            } else if (attachment.out.remaining() == 0) {
                if (attachment.peer == null && !attachment.awaits) {
                    close(key);
                } else {
                    attachment.out.clear();
                    attachment.peer.interestOps(attachment.peer.interestOps() | SelectionKey.OP_READ);
                    key.interestOps(key.interestOps() ^ SelectionKey.OP_WRITE);
                }
            }
        }
    }

    private void connect(SelectionKey key) throws IOException {
        logger.log("connect");
        SocketChannel channel = ((SocketChannel) key.channel());
        Attachment attachment = ((Attachment) key.attachment());
        System.out.println(InetAddress.getByAddress(attachment.resolvedAddress) + ":" + attachment.port);
        if (attachment.unresolvedAddress != null)
            System.out.println(new String(attachment.unresolvedAddress, StandardCharsets.UTF_8));
        channel.finishConnect();
        attachment.in = ByteBuffer.allocate(bufferSize);
        attachment.in.put(makeOK(key)).flip();
        attachment.out = ((Attachment) attachment.peer.attachment()).in;
        ((Attachment) attachment.peer.attachment()).out = attachment.in;
        attachment.peer.interestOps(SelectionKey.OP_WRITE | SelectionKey.OP_READ);
        key.interestOps(0);
    }

    private void close(SelectionKey key) {
        if (!((Attachment) key.attachment()).isDns) {
            logger.log("close");
            key.cancel();
            try {
                key.channel().close();
                SelectionKey peerKey = ((Attachment) key.attachment()).peer;
                if (peerKey != null) {
                    if ((peerKey.interestOps() & SelectionKey.OP_WRITE) == 0 && ((Attachment) peerKey.attachment()).out != null) {
                        ((Attachment) peerKey.attachment()).out.flip();
                    }
                    ((Attachment) peerKey.attachment()).peer = null;
                    peerKey.interestOps(SelectionKey.OP_WRITE);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        Proxy server = new Proxy();
        server.host = "127.0.0.1";
        server.port = 1080; //default
        if (args.length > 0)
            server.port = Integer.parseInt(args[0]);
        server.run();
    }
}