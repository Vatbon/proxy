import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

class Logger {

    private final OutputStream outputStream;
    private final SimpleDateFormat dateFormat;

    public Logger(OutputStream outputStream) {
        this.outputStream = outputStream;
        dateFormat = new SimpleDateFormat("HH:mm:ss.SSS ");
    }

    public void log(String str) {
        try {
            StringBuilder stringBuilder;
            if (System.getProperty("os.name").equals("Linux")) {
                stringBuilder = new StringBuilder().append("\033[0;34m").append(dateFormat.format(new Date())).append("\033[0m").append(": ").append(str);
            } else {
                stringBuilder = new StringBuilder().append(dateFormat.format(new Date())).append(": ").append(str);
            }
            outputStream.write(stringBuilder.append("\n").toString().getBytes());
        } catch (IOException ignored) {
        }
    }
}
