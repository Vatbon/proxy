import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Random;

class Resolver {

    public static byte[] makeAMessage(byte[] host) {
        byte[] id = new byte[2];
        new Random().nextBytes(id);
        byte[] header = new byte[]{id[0], id[1],
                0x01, 0x00,
                0x00, 0x01,
                0x00, 0x00,
                0x00, 0x00,
                0x00, 0x00};
        ByteBuffer byteBuffer = ByteBuffer.allocate(1007);
        byteBuffer.put(header);
        int i, len, pos;
        for (i = 0, len = 0, pos = 0; i < host.length; i++) {
            if (host[i] != '.') {
                len++;
            } else {
                byteBuffer.put((byte) len).put(Arrays.copyOfRange(host, pos, pos + len));
                pos = i + 1;
                len = 0;
            }
        }
        byteBuffer.put((byte) len).put(Arrays.copyOfRange(host, pos, pos + len));
        byteBuffer.put(new byte[]{0x00, 0x00, 0x01, 0x00, 0x01});
        byte[] result = new byte[header.length + host.length + 6];
        byteBuffer.position(0).get(result, 0, result.length);
        return result;
    }

    public static byte[] getResolvedAddress(byte[] data) {
        try {
            DataInputStream din = new DataInputStream(new ByteArrayInputStream(data));
            din.skipBytes(12);
            while (din.readByte() > 0) {
            }
            din.skipBytes(14);
            short addrLen;
            while((addrLen = din.readShort()) != 4)
                din.skipBytes(addrLen + 10);
            byte[] addr = new byte[addrLen];
            for (int i = 0; i < addrLen; i++) {
                addr[i] = din.readByte();
            }
            return addr;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new byte[0];
    }

    public static byte[] getDNSName(byte[] data) {
        try {
            DataInputStream din = new DataInputStream(new ByteArrayInputStream(data));
            ByteBuffer addrBuf = ByteBuffer.allocate(1024);
            din.skipBytes(12);
            int recLen;
            int len = 0;
            while ((recLen = din.readByte()) > 0) {
                byte[] record = new byte[recLen];
                len += recLen + 1;

                for (int i = 0; i < recLen; i++) {
                    record[i] = din.readByte();
                }
                addrBuf.put(record);
                addrBuf.put(".".getBytes());
            }
            byte[] dns = new byte[len - 1];
            addrBuf.position(0).get(dns);
            return dns;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }
}
